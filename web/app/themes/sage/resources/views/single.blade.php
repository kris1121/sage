@extends('layouts.app')

<?php
  function my_func($data) {
    echo '<pre>';
      var_dump($data);
    echo '<pre>'; 
    
    die();
  }
?>

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-single-'.get_post_type())
  @endwhile
  <h1>hello from single Page</h1>

  <?php

    // $wp_user = wp_get_current_user();
    
    // if($wp_user->has_cap('administrator')) {
    //   my_func('you have administrator privilages');
    // }

    $query = new WP_Query([
      'post_type'=> 'post',
      'category_name'=> 'cooking'
    ]);

    my_func($query );

  ?>

@endsection
